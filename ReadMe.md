## Hotel Reservation, final project at the end of Java course at Wantsome

### 1. Description

This is a web application for hotel management. It allows the user, meaning
for example a hotel receptionist, to have an overview and quickly add or delete
the reservations of the hotel in a few clicks. The hotel managements system 
is grouped in three sections: rooms, reservations and clients. 

#### Supported actions

##### Rooms:
1.	Check room availability for a custom set period of time;
2.	Check room reservations for a custom set period of time;
3.	Check room availability and reservations by room type;
4.	Add new reservation + Search existing clients / Add new client;
5.	Cancel reservation.

##### Reservations:
1.	Check existing client reservation for a custom set period of time;
2.	Filter the existing reservations by different filters like: room type,
  client name, canceled or still available, past or present reservations;
3.	Cancel reservations.

##### Clients:
1.	Check clients' overview / reports for a custom set period of time;
2.	Filter clients by time of the reservations;
3.	Add new clients;
4.	Edit clients data.

---
### 2. Setup

No setup needed, just start the application. If the database is missing
(like on first startup), it will create a new database (of type SQLite,
stored in a local file named 'hotel_reservation.db'), and use it to save
the future data.

Once the web app starts, navigate with a web browser at url: <http://localhost:4567>.

---
### 3. Technical details

##### User interface

The project includes a web interface. It starts with HRWebApp class.

##### Technologies

- main code is written in Java (minimum version: 8);
- it uses a small embedded database of type SQLite, using SQL and JDBC to
  connect the Java code to it;
- it uses Spark micro web framework (which includes an embedded web server, Jetty);
- web pages: using the Velocity templating engine, to separate the UI code 
  from Java code; UI code consists of basic HTML and CSS code (and a little
  Javascript);
- includes some unit tests for DB part (using JUnit library).

##### Code structure

- java code is organized in packages by its role, on layers:
  - db - database part, including DTOs and DAOs, as well as the code to init
    and connect to the db;
  - ui - code related to the interface/presentation layer;
  - root package - the main class for web interface.

- web resources are found in `main/resources` subfolders:
  - `/public` - the folder contains css files used by the web server;
  - `/public/icons` - the folder contains images and icons used by the web server;
  - `/public/csv` - the folder contains the data about rooms and some initial
    data about reservations and clients (to show the application's functionality);
  - the folder `/resources` contains the Velocity templates.

---
### 4. Follow up

On the follow up of the project, we can add a reports section. We could have
reports for which are the most occupied month of the year, which are
the favourite rooms of the clients, or report finding out where our most clients
are coming from. Also, we can this way find out which are our returning customers,
we can separate customers by stars for example, and then offer maybe a promotion
to our most loyal customers. 

Also, we can improve the customer journey after the reservation. In the application
we can have a section for the confirmation of payment for the reservation and
the method and date of payment.  

Also, we can have in the system more information about the reservations. Maybe
the client will arrive at a later hour or maybe he will want some additional
services. The reservation can have attached a small section about which additional
services the client will want: for example: air shuttle, breakfast, dinner,
private driver etc.
