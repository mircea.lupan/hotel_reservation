<html>
<head>
    <title>
        Hotel Reservation
    </title>
    <link rel = "stylesheet" type = "text/css" href = "/reservations.css">
</head>

<body>

    <div class = "rectangle1">
        <img class = "logo" src = "/icons/logo.svg">

        <div class = "menu_flex">
            <button class = "menu_button menu_button_inactive"
                    onclick = 'location.href = "/rooms";' >
                <img src = "/icons/room_icon_white.svg">
                <div>Rooms</div>
            </button>

            <button class = "menu_button menu_button_active" >
                <img src = "/icons/reservation_icon_black.svg">
                <div>Reservations</div>
            </button>

            <button class = "menu_button menu_button_inactive"
                    onclick = 'location.href = "/clients";' >
                <img src = "/icons/client_icon_white.svg">
                <div>Clients</div>
            </button>
        </div>
    </div>

    <div class = "rectangle2">
        <div class = "text21">Welcome!</div>
        <div class = "text22">Today's date is $currentDate</div>
        <div class = "text23">See reservations from</div>
        <div class = "text24">to</div>

        <form action='/reservations'>
        <div class = "rectangle21">
            <input class = "date" type="date" name="startDate" value="$startDate"
                        onchange = 'document.getElementById("submitBtn").click();' >
        </div>

        <div class = "rectangle22">
            <input class = "date" type="date" name="endDate" value="$endDate"
                        onchange = 'document.getElementById("submitBtn").click();' >
            <button type="submit" id = "submitBtn" style = "visibility:hidden;"> Select </button>
        </div>
        </form>

        <div class = "rectangle23">
            <div class = "count_text">Total</div>
            <div class = "count_value">$totalCount</div>
        </div>

        <div class = "rectangle24">
            <div class = "count_text">Cancelled</div>
            <div class = "count_value">$cancelledCount</div>
        </div>
    </div>

    <div class = "rectangle3">
        <div class = "text30 text31">Select room type</div>

        <button class = "room_type_button single_button #if($roomTypeFilter=='SINGLE') room_type_selected #end"
                onclick = 'location.href = "/reservations?roomTypeFilter=SINGLE";'>
            Single
        </button>

        <button class = "room_type_button double_button #if($roomTypeFilter=='DOUBLE') room_type_selected #end"
                onclick = 'location.href = "/reservations?roomTypeFilter=DOUBLE";'>
            Double
        </button>

        <button class = "room_type_button twin_button #if($roomTypeFilter=='TWIN') room_type_selected #end"
                onclick = 'location.href = "/reservations?roomTypeFilter=TWIN";'>
            Twin
        </button>

        <button class = "room_type_button triple_button #if($roomTypeFilter=='TRIPLE') room_type_selected #end"
                onclick='location.href = "/reservations?roomTypeFilter=TRIPLE";'>
            Triple
        </button>

        <button class = "room_type_button quad_button #if($roomTypeFilter=='QUAD') room_type_selected #end"
                onclick='location.href = "/reservations?roomTypeFilter=QUAD";'>
            Quad
        </button>

        <button class = "room_type_button all_button #if($roomTypeFilter=='ALL') room_type_selected #end"
                onclick='location.href = "/reservations?roomTypeFilter=ALL";'>
            All
        </button>

        <div class = "text30 text32">See reservations by client name</div>

        <form action = "/reservations">
            <select class = "dropdown dropdown_text" id = "clients" name = "clientFilter">
                    <option value = "ALL">All</option>
                #foreach($client in $clients)
                    <option value = "$client.name" #if($clientFilter == $client.name) selected #end >$client.name</option>
                #end
            </select>
            <input class = "dropdown_button_select" type = "Submit" value = "Select">
        </form>

        #if($hideCancelled)
            <button class = "filter_button filter_button1 textcolor_hide"
                     onclick = 'location.href = "/reservations?hideCancelled=false";'>
                <img src = "/icons/hide.svg">&ensp;
                <div>Cancelled reservations</div>
            </button>
        #else
            <button class = "filter_button filter_button1 textcolor_show"
                     onclick = 'location.href = "/reservations?hideCancelled=true";'>
                <img src = "/icons/show.svg">&ensp;
                <div>Cancelled reservations</div>
            </button>
        #end

        #if($hidePast)
            <button class = "filter_button filter_button2 textcolor_hide"
                     onclick = 'location.href = "/reservations?hidePast=false";'>
                <img src = "/icons/hide.svg">&ensp;
                <div>Past reservations</div>
            </button>
        #else
            <button class = "filter_button filter_button2 textcolor_show"
                     onclick = 'location.href = "/reservations?hidePast=true";'>
                <img src = "/icons/show.svg">&ensp;
                <div>Past reservations</div>
            </button>
        #end
    </div>

    <div class = "rectangle4">
        <table class = "roomsTable">
            <tr>
                <th> No. </th>
                <th> Client </th>
                <th> Room no. </th>
                <th> Room type </th>
                <th> Invoiced </th>
                <th> Check in </th>
                <th> Check out </th>
                <th> Reserved on </th>
                <th> Cancelled on </th>
                <th> Info </th>
                <th> Manage </th>
            </tr>

        #set($nr = 1)
        #foreach($reservation in $reservations)
            <tr #if($reservation.cancellationDate) style='color: #C5C9D1;' #else style='color: #838E9F;' #end >
                <td style = "font-weight:bold">
                    $nr.
                </td>

                <td>
                    $reservation.clientName
                </td>

                <td>
                    $reservation.roomId
                </td>

                <td>
                    $reservation.roomTypeDescription
                </td>

                <td>
                    #if(!$reservation.cancellationDate)
                        $reservation.invoice
                    #end
                </td>

                <td>
                    $reservation.startDate
                </td>

                <td>
                    $reservation.endDate
                </td>

                <td>
                    $reservation.reservationDate
                </td>

                <td>
                    #if($reservation.cancellationDate)
                        $reservation.cancellationDate
                    #end
                </td>

                <td>
                    $reservation.extraInfo
                </td>

                <td>
                    #if(!$reservation.cancellationDate)
                        #if($reservation.startDate.compareTo($currentDate) > 0)
                            <a href='/cancel_reservation?reservationId=$reservation.id'>
                                <div style = "width: 80px;">Cancel</div>
                            </a>
                        #end
                    #else
                        <div style = "width: 80px;">Cancelled</div>
                    #end
                </td>
            #set($nr = $nr + 1)
            </tr>
        #end

        </table>
        <div></div>

</body>
</html>
