package hotel_reservation;

import hotel_reservation.db.service.DbInitService;
import hotel_reservation.ui.web.*;
import spark.Spark;

import static spark.Spark.*;

public class HotelReservation {

    public static void main(String[] args) {
        DbInitService.createTablesAndInitialData();
        configureAndStartWebServer();
    }

    private static void configureAndStartWebServer() {

        staticFileLocation("/public"); //location of static resources (like images, .css ..), relative to /resources dir

        //--- ROOMS ---//
        get("/rooms", RoomsController::showRooms);

        //--- RESERVATIONS ---//
        get("/reservations", ReservationController::showReservations);

        get("/add_reservation", AddReservationController::showAddForm);
        post("/add_reservation/selectClient", AddReservationController::handleSelectClientRequest);
        post("/add_reservation/submit", AddReservationController::handleSubmitRequest);

        get("/cancel_reservation", CancelReservationController::showCancelForm);
        get("/cancel_reservation/reservations/:reservationId", CancelReservationController::handleCancelRequestFromReservationsPage);
        get("/cancel_reservation/rooms", CancelReservationController::handleCancelRequestFromRoomsPage);

        //--- CLIENTS ---//
        get("/clients", ClientController::showClients);

        get("/add_client", AddClientController::showAddForm);
        post("/add_client", AddClientController::handleAddUpdateRequest);

        get("/update_client/:id", AddClientController::showUpdateForm);
        post("/update_client/:id", AddClientController::handleAddUpdateRequest);

        //basic error handling (to catch/handle any uncaught exceptions)
        exception(Exception.class, (exception, request, response) ->
                response.body("<h2>An unexpected error occurred</h2>" +
                        "Details: " + exception.getMessage() + " <br><br>" +
                        "<a href='/rooms'> Go to rooms page </a>"));

        //--- Default/start page ---//
        redirect.get("/", "/rooms");

        //wait for server to start, then print main url
        Spark.awaitInitialization();
        System.out.println("Web app started: http://localhost:" + port());
    }

}
