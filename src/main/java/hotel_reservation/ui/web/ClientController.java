package hotel_reservation.ui.web;

import hotel_reservation.db.dto.ClientFullDto;
import hotel_reservation.db.dto.enums.TimeDescription;
import hotel_reservation.db.service.ClientDao;
import spark.Request;
import spark.Response;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static hotel_reservation.ui.web.RoomsController.getTimeAtStartOfDay;
import static hotel_reservation.ui.web.SparkUtil.render;

public class ClientController {

    private static final ClientDao clientDao = new ClientDao();

    public static String showClients(Request request, Response response) {

        Date currentDate = new Date(getTimeAtStartOfDay(System.currentTimeMillis()));

        TimeDescription timeFilter = getClientStateFilterFromParamOrSes(request);
        boolean showOnlyLast = getShowOnlyLastFromParamOrSes(request);

        List<ClientFullDto> allClients = (showOnlyLast) ? clientDao.getAllLast() : clientDao.getFullAll();
        List<ClientFullDto> clients = getClientsToDisplay(allClients, timeFilter, currentDate);

        int totalCount = allClients.size();
        int formerCount = getFormerCount(allClients, currentDate);
        int currentCount = getCurrentCount(allClients, currentDate);
        int futureCount = getFutureCount(allClients, currentDate);

        Map<String, Object> model = new HashMap<>();
        model.put("clients", clients);

        model.put("totalCount", totalCount);
        model.put("formerCount", formerCount);
        model.put("currentCount", currentCount);
        model.put("futureCount", futureCount);

        model.put("timeFilter", timeFilter);
        model.put("showOnlyLast", showOnlyLast);
        model.put("currentDate", currentDate);
        return render(model, "clients.vm");
    }

    private static List<ClientFullDto> getClientsToDisplay(
            List<ClientFullDto> allClients, TimeDescription timeFilter, Date currentDate) {
        return allClients.stream()
                .filter(r -> (timeFilter == TimeDescription.ALL) ||
                        ((timeFilter == TimeDescription.FORMER) &&
                                (r.getEndDate() != null) &&
                                (r.getEndDate().before(currentDate) || r.getEndDate().equals(currentDate))) ||
                        ((timeFilter == TimeDescription.CURRENT) &&
                                (r.getStartDate() != null) && (r.getEndDate() != null) &&
                                ((r.getStartDate().before(currentDate) || r.getStartDate().equals(currentDate)) &&
                                        (r.getEndDate().after(currentDate) || r.getEndDate().equals(currentDate)))) ||
                        (timeFilter == TimeDescription.FUTURE &&
                                ((r.getStartDate() == null) ||
                                        (r.getStartDate() != null) && r.getStartDate().after(currentDate))))
                .collect(Collectors.toList());
    }

    private static int getFormerCount(List<ClientFullDto> clients, Date currentDate) {
        int count = 0;
        for (ClientFullDto client : clients) {
            if ((client.getEndDate() != null) &&
                    (client.getEndDate().before(currentDate) || client.getEndDate().equals(currentDate))) {
                count++;
            }
        }
        return count;
    }

    private static int getCurrentCount(List<ClientFullDto> clients, Date currentDate) {
        int count = 0;
        for (ClientFullDto client : clients) {
            if ((client.getStartDate() != null) && (client.getEndDate() != null) &&
                    ((client.getStartDate().before(currentDate) || client.getStartDate().equals(currentDate)) &&
                            (client.getEndDate().after(currentDate) || client.getEndDate().equals(currentDate)))) {
                count++;
            }
        }
        return count;
    }

    private static int getFutureCount(List<ClientFullDto> clients, Date currentDate) {
        int count = 0;
        for (ClientFullDto client : clients) {
            if ((client.getStartDate() == null) ||
                    ((client.getStartDate() != null) && client.getStartDate().after(currentDate))) {
                count++;
            }
        }
        return count;
    }

    private static TimeDescription getClientStateFilterFromParamOrSes(Request request) {
        String param = request.queryParams("timeFilter");
        if (param != null) {
            request.session().attribute("timeFilter", param);
        } else {
            param = request.session().attribute("timeFilter");
        }
        if (param != null)
            return TimeDescription.valueOf(param);
        else
            return TimeDescription.ALL;
    }

    private static boolean getShowOnlyLastFromParamOrSes(Request request) {
        String param = request.queryParams("showOnlyLast");
        if (param != null) {
            request.session().attribute("showOnlyLast", param);
        } else {
            param = request.session().attribute("showOnlyLast");
        }
        if (param != null)
            return Boolean.parseBoolean(param);
        else
            return true;
    }
}
