package hotel_reservation.ui.web;

import hotel_reservation.db.dto.ReservationDto;
import hotel_reservation.db.dto.ReservationFullDto;
import hotel_reservation.db.service.ClientDao;
import hotel_reservation.db.service.ReservationDao;
import hotel_reservation.db.service.RoomDao;
import hotel_reservation.db.service.RoomTypeDao;
import spark.Request;
import spark.Response;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static hotel_reservation.ui.web.AddReservationController.getRoomNumberFromParamOrSes;
import static hotel_reservation.ui.web.RoomsController.getEndDateFromParamOrSes;
import static hotel_reservation.ui.web.RoomsController.getStartDateFromParamOrSes;
import static hotel_reservation.ui.web.SparkUtil.render;

public class CancelReservationController {

    private static final RoomDao roomDao = new RoomDao();
    private static final RoomTypeDao roomTypeDao = new RoomTypeDao();
    private static final ClientDao clientDao = new ClientDao();
    private static final ReservationDao reservationDao = new ReservationDao();

    public static String showCancelForm(Request request, Response response) {

        int reservationId = getReservationIdFromParamOrSes(request);

        Optional<ReservationDto> optionalReservation = reservationDao.get(reservationId);
        if (optionalReservation.isPresent()) {
            ReservationDto reservation = optionalReservation.get();

            String clientName = clientDao.get(reservation.getClientId()).get().getName();
            int roomNumber = reservation.getRoomId();
            String roomTypeDescription = roomTypeDao.getById(roomDao.get(reservation.getRoomId()).get().getRoomType()).get().getRoomTypeDescription().toString();
            BigDecimal roomRate = roomTypeDao.getById(roomDao.get(reservation.getRoomId()).get().getRoomType()).get().getRate();
            Date checkInDate = reservation.getStartDate();
            Date checkOutDate = reservation.getEndDate();
            int dayCount = (int) ((checkOutDate.getTime() - checkInDate.getTime()) / (24L * 60 * 60 * 1000));
            BigDecimal totalPrice = roomRate.multiply(BigDecimal.valueOf(dayCount));

            Map<String, Object> model = new HashMap<>();

            model.put("reservationId", reservationId);
            model.put("clientName", clientName);
            model.put("roomNumber", roomNumber);
            model.put("roomTypeDescription", roomTypeDescription);
            model.put("roomRate", roomRate);
            model.put("roomInfo", roomDao.get(reservation.getRoomId()).get().getInfo());
            model.put("checkInDate", checkInDate);
            model.put("checkOutDate", checkOutDate);
            model.put("dayCount", dayCount);
            model.put("totalPrice", totalPrice);

            return render(model, "cancel_reservation.vm");
        }

        return "";
    }

    public static Object handleCancelRequestFromReservationsPage(Request request, Response response) {
        int reservationId = Integer.parseInt(request.params("reservationId"));
        reservationDao.cancel(reservationId);
        response.redirect("/reservations");
        return response;
    }

    public static Object handleCancelRequestFromRoomsPage(Request request, Response response) {

        int roomNumber = getRoomNumberFromParamOrSes(request);
        Date checkInDate = getStartDateFromParamOrSes(request);
        Date checkOutDate = getEndDateFromParamOrSes(request);

        List<ReservationFullDto> reservations = reservationDao.getAll();
        int reservationId = 0;
        for (ReservationFullDto reservation : reservations) {
            if ((reservation.getRoomId() == roomNumber) &&
                    (reservation.getStartDate().equals(checkInDate)) && (reservation.getEndDate().equals(checkOutDate))) {
                reservationId = reservation.getId();
                break;
            }
        }
        response.redirect("/cancel_reservation?reservationId=" + reservationId);
        return response;
    }

    private static int getReservationIdFromParamOrSes(Request request) {
        String param = request.queryParams("reservationId");
        if (param != null) {
            request.session().attribute("reservationId", param);
        } else {
            param = request.session().attribute("reservationId");
        }
        if (param != null)
            return Integer.parseInt(param);
        else
            return 0;
    }

}
