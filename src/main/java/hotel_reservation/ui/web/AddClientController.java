package hotel_reservation.ui.web;

import hotel_reservation.db.dto.ClientDto;
import hotel_reservation.db.service.ClientDao;
import spark.Request;
import spark.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static hotel_reservation.ui.web.SparkUtil.render;

public class AddClientController {

    private static final ClientDao clientDao = new ClientDao();

    public static String showAddForm(Request request, Response response) {
        String name = request.queryParams("name");
        if (name == null) name = "";
        return renderAddUpdateForm("", name, "", "", "", "");
    }

    public static String showUpdateForm(Request request, Response response) {
        String id = request.params("id");
        Optional<ClientDto> optClient = clientDao.get(Integer.parseInt(id));
        if (optClient.isPresent()) {
            ClientDto client = optClient.get();
            return renderAddUpdateForm(
                    String.valueOf(client.getId()),
                    "'" + client.getName() + "'",
                    client.getPhoneNumber(),
                    client.getEmail(),
                    "'" + client.getAddress() + "'",
                    "");
        }
        throw new RuntimeException("Client " + id + " not found!");
    }

    private static String renderAddUpdateForm(
            String id, String name, String phone, String email, String address,
            String errorMessage) {

        Map<String, Object> model = new HashMap<>();
        model.put("id", id);
        model.put("name", name);
        model.put("phone", phone);
        model.put("email", email);
        model.put("address", address);
        model.put("errorMsg", errorMessage);
        model.put("isUpdate", id != null && !id.isEmpty());
        return render(model, "add_client.vm");
    }

    public static Object handleAddUpdateRequest(Request request, Response response) {

        String id = request.queryParams("id");
        String name = request.queryParams("name").replace("'", "");
        String phone = request.queryParams("phone");
        String email = request.queryParams("email");
        String address = request.queryParams("address");

        boolean isUpdateCase = (id != null) && (!id.isEmpty());

        ClientDto client = validateAndBuildClient(id, name, phone, email, address);

        try {
            if (isUpdateCase) {
                clientDao.update(client);
                response.redirect("/clients");
            } else {
                clientDao.insert(client);
                response.redirect("/clients");
                //ToDo - de facut redirect catre clienti sau catre add reservation
                //in functie de unde este creat clientul nou
            }
            return response;

        } catch (Exception e) {
            String errorMessage = e.getMessage().contains("UNIQUE constraint failed") ?
                    "Error: a client with same data already exists" :
                    e.getMessage();
            return renderAddUpdateForm(id, name, phone, email, address, errorMessage);
        }
    }

    private static ClientDto validateAndBuildClient(String id, String name, String phone, String email, String address) {

        int idValue = (id != null) && (!id.isEmpty()) ? Integer.parseInt(id) : -1;

        if (name == null || name.isEmpty()) {
            throw new RuntimeException("Client name is required!");
        }

        if (phone == null || phone.isEmpty()) {
            throw new RuntimeException("Client phone number is required!");
        }

        if (email == null || email.isEmpty()) {
            throw new RuntimeException("Client email is required!");
        }

        if (address == null || address.isEmpty()) {
            throw new RuntimeException("Client name is required!");
        }

        return new ClientDto(idValue, name, phone, email, address);
    }

}
