package hotel_reservation.ui.web;

import hotel_reservation.db.dto.ClientDto;
import hotel_reservation.db.dto.ReservationDto;
import hotel_reservation.db.dto.RoomDto;
import hotel_reservation.db.dto.RoomTypeDto;
import hotel_reservation.db.dto.enums.RoomTypeDescription;
import hotel_reservation.db.service.ClientDao;
import hotel_reservation.db.service.ReservationDao;
import hotel_reservation.db.service.RoomDao;
import hotel_reservation.db.service.RoomTypeDao;
import spark.Request;
import spark.Response;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static hotel_reservation.ui.web.RoomsController.*;
import static hotel_reservation.ui.web.SparkUtil.render;

public class AddReservationController {

    private static final RoomDao roomDao = new RoomDao();
    private static final RoomTypeDao roomTypeDao = new RoomTypeDao();
    private static final ClientDao clientDao = new ClientDao();
    private static final ReservationDao reservationDao = new ReservationDao();

    public static String showAddForm(Request request, Response response) {

        Date currentDate = new Date(getTimeAtStartOfDay(System.currentTimeMillis()));

        int roomNumber = getRoomNumberFromParamOrSes(request);

        RoomDto room = roomDao.get(roomNumber).get();
        RoomTypeDto roomType = roomTypeDao.getById(room.getRoomType()).get();

        RoomTypeDescription roomTypeDescription = roomType.getRoomTypeDescription();
        BigDecimal roomRate = roomType.getRate();
        String roomInfo = room.getInfo();
        Date checkInDate = getStartDateFromParamOrSes(request);
        checkInDate = validateCheckInDate(checkInDate, currentDate);
        Date checkOutDate = getEndDateFromParamOrSes(request);

        int selectedClientId = Integer.parseInt(request.queryParamOrDefault("selectedClientId", "-1"));

        int dayCount = (int) ((checkOutDate.getTime() - checkInDate.getTime()) / (24L * 60 * 60 * 1000));
        BigDecimal totalPrice = roomRate.multiply(BigDecimal.valueOf(dayCount));

        List<ClientDto> clients = clientDao.getAll();
        Optional<ClientDto> optClient = clientDao.get(selectedClientId);
        ClientDto selectedClient = (selectedClientId > 0) ? optClient.get() : null;
        String selectedClientName = (selectedClientId > 0) ? optClient.get().getName() : "";

        Map<String, Object> model = new HashMap<>();
        model.put("roomNumber", roomNumber);
        model.put("roomTypeDescription", roomTypeDescription);
        model.put("roomRate", roomRate);
        model.put("roomInfo", roomInfo);
        model.put("checkInDate", checkInDate);
        model.put("checkOutDate", checkOutDate);
        model.put("dayCount", dayCount);
        model.put("totalPrice", totalPrice);

        model.put("clients", clients);
        model.put("selectedClient", selectedClient);
        model.put("selectedClientName", selectedClientName);

        return render(model, "add_reservation.vm");
    }

    private static Date validateCheckInDate(Date checkInDate, Date currentDate) {
        return (checkInDate.before(currentDate)) ? currentDate : checkInDate;
    }

    public static Object handleSelectClientRequest(Request request, Response response) {

        String name = request.queryParams("clientsList");

        List<ClientDto> clients = clientDao.getAll();
        Optional<ClientDto> optClient = clients.stream().filter(r -> r.getName().equals(name)).findFirst();
        int clientId = optClient.map(ClientDto::getId).orElse(-1);

        if (clientId > 0)
            response.redirect("/add_reservation?" +
                    "roomNumber=" + request.queryParams("roomNumber") + "&" +
                    "checkInDate=" + request.queryParams("checkInDate") + "&" +
                    "checkOutDate=" + request.queryParams("checkOutDate") + "&" +
                    "selectedClientId=" + clientId
            );
        else {
            response.redirect("/add_client?name='" + name + "'");
        }

        return "";
    }

    public static Object handleSubmitRequest(Request request, Response response) {
        ReservationDto reservationDto = new ReservationDto(-1,
                Integer.parseInt(request.queryParams("clientId")),
                Integer.parseInt(request.queryParams("roomNumber")),
                Date.valueOf(request.queryParams("checkInDate")),
                Date.valueOf(request.queryParams("checkOutDate")),
                new Date(getTimeAtStartOfDay(System.currentTimeMillis())),
                null,
                "");
        reservationDao.insert(reservationDto);
        response.redirect("/rooms");
        return "";
    }

    private static String getStringVariableFromParamOrSes(Request request, String strVar, String defaultValue) {
        String param = request.queryParams(strVar);
        if (param != null) {
            request.session().attribute(strVar, param);
        } else {
            param = request.session().attribute(strVar);
        }
        if (param != null)
            return param;
        else
            return defaultValue;
    }

    public static int getRoomNumberFromParamOrSes(Request request) {
        String param = request.queryParams("roomNumber");
        if (param != null) {
            request.session().attribute("roomNumber", param);
        } else {
            param = request.session().attribute("roomNumber");
        }
        if (param != null)
            return Integer.parseInt(param);
        else
            return 0;
    }
}
