package hotel_reservation.ui.web;

import hotel_reservation.db.dto.RoomFullDto;
import hotel_reservation.db.dto.RoomTypeDto;
import hotel_reservation.db.dto.enums.RoomStateDescription;
import hotel_reservation.db.dto.enums.RoomTypeFilter;
import hotel_reservation.db.service.RoomDao;
import hotel_reservation.db.service.RoomTypeDao;
import spark.Request;
import spark.Response;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static hotel_reservation.ui.web.SparkUtil.render;

public class RoomsController {

    private static final RoomDao roomDao = new RoomDao();
    private static final RoomTypeDao roomTypeDao = new RoomTypeDao();

    public static String showRooms(Request request, Response response) {

        Date currentDate = new Date(getTimeAtStartOfDay(System.currentTimeMillis()));
        Date maxEndDate = new Date(currentDate.getTime() + 10L * 365 * 24 * 60 * 60 * 1000);   //max 10 years

        RoomTypeFilter roomTypeFilter = getRoomTypeFromParamOrSes(request);
        RoomStateDescription roomStateFilter = getRoomStateFilterFromParamOrSes(request);
        Date startDateRoom = getStartDateFromParamOrSes(request);
        Date endDateRoom = getEndDateFromParamOrSes(request);
        endDateRoom = validateEndDate(startDateRoom, endDateRoom, currentDate);

        List<RoomTypeDto> roomTypes = roomTypeDao.getAll();

        List<RoomFullDto> roomsFromDatabase = roomDao.getAll();
        List<RoomFullDto> allRooms = getRoomsAvailability(roomsFromDatabase, startDateRoom, endDateRoom, currentDate, maxEndDate);
        List<RoomFullDto> rooms = getRoomsToDisplay(allRooms, roomTypeFilter, roomStateFilter, startDateRoom, endDateRoom);

        int totalCount = roomsFromDatabase.size();
        int availableCount = getAvailableCount(allRooms, currentDate);
        int partiallyCount = getPartiallyCount(allRooms, currentDate);

        Map<String, Object> model = new HashMap<>();
        model.put("rooms", rooms);
        model.put("roomTypes", roomTypes);

        model.put("totalCount", totalCount);
        model.put("availableCount", availableCount);
        model.put("partiallyCount", partiallyCount);
        model.put("unavailableCount", totalCount - availableCount - partiallyCount);

        model.put("roomTypeFilter", roomTypeFilter);
        model.put("roomStateFilter", roomStateFilter);
        model.put("startDateRoom", startDateRoom);
        model.put("endDateRoom", endDateRoom);
        model.put("currentDate", currentDate);
        model.put("maxEndDate", maxEndDate);

        return render(model, "rooms.vm");
    }

    private static List<RoomFullDto> getRoomsAvailability(List<RoomFullDto> rooms, Date startDate, Date endDate, Date currentDate, Date maxEndDate) {
        if (startDate == null) {
            startDate = currentDate;
        }
        if (endDate == null) {
            endDate = maxEndDate;
        }
        int dayCount = getIndexFromDate(endDate, startDate);
        List<Boolean> availableDays = new ArrayList<>(dayCount);
        for (int j = 0; j < dayCount; j++) {
            availableDays.add(true);
        }
        List<RoomFullDto> result = new ArrayList<>();
        int id = 1;
        for (int i = 0; i < rooms.size(); i++) {
            if ((i < rooms.size() - 1) && (rooms.get(i).getNumber() == rooms.get(i + 1).getNumber())) {
                setDaysUnavailable(availableDays, rooms.get(i).getStartDate(), rooms.get(i).getEndDate(), startDate, endDate);
            } else {
                setDaysUnavailable(availableDays, rooms.get(i).getStartDate(), rooms.get(i).getEndDate(), startDate, endDate);

                int fromDay = 0;
                while (fromDay < availableDays.size()) {
                    int toDay = fromDay;
                    while ((toDay < availableDays.size()) && (availableDays.get(toDay) == availableDays.get(fromDay))) {
                        toDay++;
                    }
                    RoomFullDto newElem = new RoomFullDto(id++,
                            rooms.get(i).getNumber(),
                            rooms.get(i).getRoomType(),
                            rooms.get(i).getRoomTypeDescription(),
                            rooms.get(i).getRoomRate(),
                            rooms.get(i).getInfo(),
                            getDateFromIndex(fromDay, startDate),
                            getDateFromIndex(toDay, startDate));
                    newElem.setAvailable(availableDays.get(fromDay));
                    result.add(newElem);
                    fromDay = toDay;
                }
                for (int j = 0; j < dayCount; j++) {
                    availableDays.set(j, true);
                }
            }
        }
        return result;
    }

    public static long getTimeAtStartOfDay(long time) {
        Date date = Date.valueOf("2020-06-01");
        long mod = (time - date.getTime()) % (24L * 60 * 60 * 1000);
        return time - mod;
    }

    private static int getIndexFromDate(Date date, Date startDate) {
        return (int) ((date.getTime() - startDate.getTime()) / (24L * 60 * 60 * 1000));
    }

    private static Date getDateFromIndex(int index, Date startDate) {
        return new Date(startDate.getTime() + 1L * index * 24 * 60 * 60 * 1000);
    }

    private static void setDaysUnavailable(List<Boolean> days, Date fromDate, Date toDate, Date startDate, Date endDate) {
        if (fromDate == null) return;
        if (fromDate.before(startDate)) fromDate = startDate;
        if (toDate.after(endDate)) toDate = endDate;
        for (int i = getIndexFromDate(fromDate, startDate); i < getIndexFromDate(toDate, startDate); i++) {
            days.set(i, false);
        }
    }

    private static List<RoomFullDto> getRoomsToDisplay(
            List<RoomFullDto> allRooms, RoomTypeFilter roomTypeFilter, RoomStateDescription roomStateFilter,
            Date startDate, Date endDate) {
        return allRooms.stream()
                .filter(r -> r.getRoomTypeDescription().equals(roomTypeFilter.toString()) ||
                        roomTypeFilter == RoomTypeFilter.ALL)
                .filter(r -> (roomStateFilter == RoomStateDescription.ALL) ||
                        (r.isAvailable() && (roomStateFilter == RoomStateDescription.AVAILABLE)) ||
                        (!r.isAvailable() && (roomStateFilter == RoomStateDescription.UNAVAILABLE)))
                .collect(Collectors.toList());
    }

    private static int getAvailableCount(List<RoomFullDto> rooms, Date currentDate) {
        Map<Integer, Boolean> availableRooms = new HashMap<>();
        Map<Integer, Boolean> unavailableRooms = new HashMap<>();
        int count = 0;
        for (RoomFullDto room : rooms) {
            availableRooms.put(room.getNumber(), false);
            unavailableRooms.put(room.getNumber(), false);
        }
        for (RoomFullDto room : rooms) {
            if (room.isAvailable()) availableRooms.put(room.getNumber(), true);
            if (!room.isAvailable()) unavailableRooms.put(room.getNumber(), true);
        }
        for (Integer i : availableRooms.keySet()) {
            if (availableRooms.get(i) && !unavailableRooms.get(i)) {
                count++;
            }
        }
        return count;
    }

    private static int getPartiallyCount(List<RoomFullDto> rooms, Date currentDate) {
        Map<Integer, Boolean> availableRooms = new HashMap<>();
        Map<Integer, Boolean> unavailableRooms = new HashMap<>();
        int count = 0;
        for (RoomFullDto room : rooms) {
            availableRooms.put(room.getNumber(), false);
            unavailableRooms.put(room.getNumber(), false);
        }
        for (RoomFullDto room : rooms) {
            if (room.isAvailable()) availableRooms.put(room.getNumber(), true);
            if (!room.isAvailable()) unavailableRooms.put(room.getNumber(), true);
        }
        for (Integer i : availableRooms.keySet()) {
            if (availableRooms.get(i) && unavailableRooms.get(i)) {
                count++;
            }
        }
        return count;
    }

    private static RoomStateDescription getRoomStateFilterFromParamOrSes(Request request) {
        String param = request.queryParams("roomStateFilter");
        if (param != null) {
            request.session().attribute("roomStateFilter", param);
        } else {
            param = request.session().attribute("roomStateFilter");
        }
        if (param != null)
            return RoomStateDescription.valueOf(param);
        else
            return RoomStateDescription.ALL;
    }

    static RoomTypeFilter getRoomTypeFromParamOrSes(Request request) {
        String param = request.queryParams("roomTypeFilter");
        if (param != null) {
            request.session().attribute("roomTypeFilter", param);
        } else {
            param = request.session().attribute("roomTypeFilter");
        }
        if (param != null)
            return RoomTypeFilter.valueOf(param);
        else
            return RoomTypeFilter.ALL;
    }

    static Date getStartDateFromParamOrSes(Request request) {
        String param = request.queryParams("startDateRoom");
        if (param != null) {
            request.session().attribute("startDateRoom", param);
        } else {
            param = request.session().attribute("startDateRoom");
        }
        if ((param != null) && (!param.isEmpty())) {
            return Date.valueOf(param);
        } else
            return null;
    }

    static Date getEndDateFromParamOrSes(Request request) {
        String param = request.queryParams("endDateRoom");
        if (param != null) {
            request.session().attribute("endDateRoom", param);
        } else {
            param = request.session().attribute("endDateRoom");
        }
        if ((param != null) && (!param.isEmpty())) {
            return Date.valueOf(param);
        } else
            return null;
    }

    static Date validateEndDate(Date startDate, Date endDate, Date currentDate) {
        if (endDate == null) return null;
        if (startDate == null) {
            if (endDate.before(currentDate)) {
                return null;
            } else {
                return endDate;
            }
        }
        if ((endDate.before(startDate)) || endDate.equals(startDate)) {
            return new Date(getTimeAtStartOfDay(startDate.getTime()) + 24L * 60 * 60 * 1000);
        }
        return endDate;
    }
}
