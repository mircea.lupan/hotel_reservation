package hotel_reservation.ui.web;

import hotel_reservation.db.dto.ClientDto;
import hotel_reservation.db.dto.ReservationFullDto;
import hotel_reservation.db.dto.RoomTypeDto;
import hotel_reservation.db.dto.enums.RoomTypeFilter;
import hotel_reservation.db.service.ClientDao;
import hotel_reservation.db.service.ReservationDao;
import hotel_reservation.db.service.RoomTypeDao;
import spark.Request;
import spark.Response;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static hotel_reservation.ui.web.RoomsController.*;
import static hotel_reservation.ui.web.SparkUtil.render;

public class ReservationController {

    private static final ReservationDao reservationDao = new ReservationDao();
    private static final RoomTypeDao roomTypeDao = new RoomTypeDao();
    private static final ClientDao clientDao = new ClientDao();

    public static String showReservations(Request request, Response response) {

        Date currentDate = new Date(getTimeAtStartOfDay(System.currentTimeMillis()));

        RoomTypeFilter roomTypeFilter = getRoomTypeFromParamOrSes(request);
        boolean hideCancelled = getHideCancelledFromParamOrSes(request);
        boolean hidePast = getHidePastFromParamOrSes(request);

        Date startDate = getStartDateFromParamOrSes(request);
        Date endDate = getEndDateFromParamOrSes(request);
        endDate = validateEndDate(startDate, endDate, currentDate);

        String clientFilter = getClientFromParamOrSes(request);

        List<RoomTypeDto> roomTypes = roomTypeDao.getAll();
        List<ClientDto> clients = clientDao.getAll();

        List<ReservationFullDto> allReservations = reservationDao.getAll();
        List<ReservationFullDto> reservations =
                getReservationsToDisplay(allReservations, roomTypeFilter, hideCancelled, hidePast, startDate, endDate, currentDate, clientFilter);

        int totalCount = allReservations.size();
        int cancelledCount = getCancelledCount(allReservations);

        Map<String, Object> model = new HashMap<>();
        model.put("reservations", reservations);
        model.put("roomTypes", roomTypes);
        model.put("clients", clients);

        model.put("totalCount", totalCount);
        model.put("cancelledCount", cancelledCount);

        model.put("roomTypeFilter", roomTypeFilter);
        model.put("hideCancelled", hideCancelled);
        model.put("hidePast", hidePast);
        model.put("startDate", startDate);
        model.put("endDate", endDate);
        model.put("currentDate", currentDate);
        model.put("clientFilter", clientFilter);

        return render(model, "reservations.vm");
    }

    private static int getCancelledCount(List<ReservationFullDto> allReservations) {
        return (int) allReservations.stream()
                .filter(r -> r.getCancellationDate() != null)
                .count();
    }

    private static List<ReservationFullDto> getReservationsToDisplay(
            List<ReservationFullDto> allReservations, RoomTypeFilter roomTypeFilter,
            boolean hideCancelled, boolean hidePast,
            Date startDate, Date endDate, Date currentDate, String clientFilter) {
        return allReservations.stream()
                .filter(r -> r.getRoomTypeDescription().equals(roomTypeFilter.toString()) ||
                        roomTypeFilter == RoomTypeFilter.ALL)
                .filter(r -> !((r.getCancellationDate() != null) && hideCancelled))
                .filter(r -> r.getEndDate().after(currentDate) || !hidePast)
                .filter(r -> ((startDate == null) || (r.getEndDate().after(startDate))) &&
                        ((endDate == null) || (r.getStartDate().before(endDate))))
                .filter(r -> r.getClientName().equals(clientFilter) || clientFilter.equals("ALL"))
                .collect(Collectors.toList());
    }

    private static boolean getHideCancelledFromParamOrSes(Request request) {
        String param = request.queryParams("hideCancelled");
        if (param != null) {
            request.session().attribute("hideCancelled", param);
        } else {
            param = request.session().attribute("hideCancelled");
        }
        return (param == null) || Boolean.parseBoolean(param);
    }

    private static boolean getHidePastFromParamOrSes(Request request) {
        String param = request.queryParams("hidePast");
        if (param != null) {
            request.session().attribute("hidePast", param);
        } else {
            param = request.session().attribute("hidePast");
        }
        return (param == null) || Boolean.parseBoolean(param);
    }

    private static String getClientFromParamOrSes(Request request) {
        String param = request.queryParams("clientFilter");
        if (param != null) {
            request.session().attribute("clientFilter", param);
        } else {
            param = request.session().attribute("clientFilter");
        }
        return (param != null) ? param : "ALL";
    }

    static Date getStartDateFromParamOrSes(Request request) {
        String param = request.queryParams("startDate");
        if (param != null) {
            request.session().attribute("startDate", param);
        } else {
            param = request.session().attribute("startDate");
        }
        if ((param != null) && (!param.isEmpty())) {
            return Date.valueOf(param);
        } else
            return null;
    }

    static Date getEndDateFromParamOrSes(Request request) {
        String param = request.queryParams("endDate");
        if (param != null) {
            request.session().attribute("endDate", param);
        } else {
            param = request.session().attribute("endDate");
        }
        if ((param != null) && (!param.isEmpty())) {
            return Date.valueOf(param);
        } else
            return null;
    }

}
