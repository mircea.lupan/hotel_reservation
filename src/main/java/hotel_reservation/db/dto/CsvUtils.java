package hotel_reservation.db.dto;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CsvUtils {

    public static List<RoomDto> loadRoomsFromCsvFile(String fileName) {

        try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
            return lines
                    .filter(s -> s.charAt(0) != '#')    //filter comments lines
                    .map(RoomDto::convertFromCSV)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public static List<RoomTypeDto> loadRoomTypesFromCsvFile(String fileName) {

        try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
            return lines
                    .filter(s -> s.charAt(0) != '#')
                    .map(RoomTypeDto::convertFromCSV)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public static List<ClientDto> loadClientsFromCsvFile(String fileName) {

        try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
            return lines
                    .filter(s -> s.charAt(0) != '#')
                    .map(ClientDto::convertFromCSV)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public static List<ReservationDto> loadReservationsFromCsvFile(String fileName) {

        try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
            return lines
                    .filter(s -> s.charAt(0) != '#')
                    .map(ReservationDto::convertFromCSV)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

}
