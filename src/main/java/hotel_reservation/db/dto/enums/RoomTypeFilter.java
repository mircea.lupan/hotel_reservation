package hotel_reservation.db.dto.enums;

public enum RoomTypeFilter {
    SINGLE, DOUBLE, TWIN, TRIPLE, QUAD, ALL
}
