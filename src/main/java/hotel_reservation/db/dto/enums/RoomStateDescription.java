package hotel_reservation.db.dto.enums;

public enum RoomStateDescription {
    AVAILABLE, UNAVAILABLE, ALL
}
