package hotel_reservation.db.dto.enums;

public enum TimeDescription {
    FORMER, CURRENT, FUTURE, ALL
}
