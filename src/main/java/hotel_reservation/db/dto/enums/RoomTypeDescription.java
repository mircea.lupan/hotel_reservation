package hotel_reservation.db.dto.enums;

public enum RoomTypeDescription {
    SINGLE, DOUBLE, TWIN, TRIPLE, QUAD
}
