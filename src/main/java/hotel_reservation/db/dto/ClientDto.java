package hotel_reservation.db.dto;

import java.util.Objects;

public class ClientDto {

    private final int id;
    private final String name;
    private final String phoneNumber;
    private final String email;
    private final String address;

    public ClientDto(int id, String name, String phoneNumber, String email, String address) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.address = address;
    }

    public static ClientDto convertFromCSV(String line) {
        int index1 = line.indexOf('"');
        int index2 = line.indexOf('"', index1 + 1);
        String firstPart = line.substring(index1 + 1, index2);   //first part is delimited by double-quote
        int index3 = line.indexOf('"', index2 + 1);
        int index4 = line.indexOf('"', index3 + 1);
        String lastPart = line.substring(index3 + 1, index4);   //last part is delimited by double-quote

        String[] parts = line.substring(index2).split("\\s*,\\s*");

        try {
            return new ClientDto(-1, firstPart, parts[1], parts[2], lastPart);
        } catch (Exception e) {
            System.out.println("failed to convert line to client: '" + line + "'" +
                    ", because: " + e);
            return null;
        }
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "ClientDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientDto clientDto = (ClientDto) o;
        return id == clientDto.id &&
                Objects.equals(name, clientDto.name) &&
                Objects.equals(phoneNumber, clientDto.phoneNumber) &&
                Objects.equals(email, clientDto.email) &&
                Objects.equals(address, clientDto.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, phoneNumber, email, address);
    }
}
