package hotel_reservation.db.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

public class RoomFullDto {

    private final int id;
    private final int number;
    private final int roomType;
    private final String roomTypeDescription;
    private final BigDecimal roomRate;
    private final String info;
    private Date startDate;
    private Date endDate;
    private boolean available;

    public RoomFullDto(int id, int number, int roomType, String roomTypeDescription, BigDecimal roomRate, String info, Date startDate, Date endDate) {
        this.id = id;
        this.number = number;
        this.roomType = roomType;
        this.roomTypeDescription = roomTypeDescription;
        this.roomRate = roomRate;
        this.info = info;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public int getRoomType() {
        return roomType;
    }

    public String getRoomTypeDescription() {
        return roomTypeDescription;
    }

    public BigDecimal getRoomRate() {
        return roomRate;
    }

    public String getInfo() {
        return info;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "RoomFullDto{" +
                "id=" + id +
                ", number=" + number +
                ", roomType=" + roomType +
                ", roomTypeDescription='" + roomTypeDescription + '\'' +
                ", roomRate=" + roomRate +
                ", info='" + info + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", available=" + available +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomFullDto that = (RoomFullDto) o;
        return id == that.id &&
                number == that.number &&
                roomType == that.roomType &&
                available == that.available &&
                Objects.equals(roomTypeDescription, that.roomTypeDescription) &&
                Objects.equals(roomRate, that.roomRate) &&
                Objects.equals(info, that.info) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number, roomType, roomTypeDescription, roomRate, info, startDate, endDate, available);
    }
}
