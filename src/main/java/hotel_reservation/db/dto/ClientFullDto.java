package hotel_reservation.db.dto;

import java.sql.Date;
import java.util.Objects;

public class ClientFullDto extends ClientDto {

    private final Date startDate;
    private final Date endDate;

    public ClientFullDto(int id, String name, String phoneNumber, String email, String address, Date startDate, Date endDate) {
        super(id, name, phoneNumber, email, address);
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    @Override
    public String toString() {
        return "ClientFullDto{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ClientFullDto that = (ClientFullDto) o;
        return Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), startDate, endDate);
    }
}
