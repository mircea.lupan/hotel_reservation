package hotel_reservation.db.dto;

import java.sql.Date;
import java.util.Objects;

public class ReservationDto {
    private final int id;
    private final int clientId;
    private final int roomId;
    private final Date startDate;
    private final Date endDate;
    private final Date reservationDate;
    private final Date cancellationDate;
    private final String extraInfo;

    public ReservationDto(
            int id, int clientId, int roomId,
            Date startDate, Date endDate,
            Date reservationDate, Date cancellationDate,
            String extraInfo) {
        this.id = id;
        this.clientId = clientId;
        this.roomId = roomId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.reservationDate = reservationDate;
        this.cancellationDate = cancellationDate;
        this.extraInfo = extraInfo;
    }

    public static ReservationDto convertFromCSV(String line) {
        int index1 = line.indexOf('"');
        int index2 = line.indexOf('"', index1 + 1);
        String lastPart = line.substring(index1 + 1, index2);   //last part is delimited by double-quote

        String[] parts = line.split("\\s*,\\s*");

        try {
            return new ReservationDto(-1, Integer.parseInt(parts[0]), Integer.parseInt(parts[1]),
                    Date.valueOf(parts[2]), Date.valueOf(parts[3]), Date.valueOf(parts[4]),
                    (parts[5].equals("")) ? null : Date.valueOf(parts[5]), lastPart);
        } catch (Exception e) {
            System.out.println("failed to convert line to reservation: '" + line + "'" +
                    ", because: " + e);
            return null;
        }
    }

    public int getId() {
        return id;
    }

    public int getClientId() {
        return clientId;
    }

    public int getRoomId() {
        return roomId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    @Override
    public String toString() {
        return "ReservationDto{" +
                "id=" + id +
                ", clientId=" + clientId +
                ", roomId=" + roomId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", reservationDate=" + reservationDate +
                ", cancellationDate=" + cancellationDate +
                ", extraInfo='" + extraInfo + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationDto that = (ReservationDto) o;
        return id == that.id &&
                clientId == that.clientId &&
                roomId == that.roomId &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(reservationDate, that.reservationDate) &&
                Objects.equals(cancellationDate, that.cancellationDate) &&
                Objects.equals(extraInfo, that.extraInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, roomId, startDate, endDate, reservationDate, cancellationDate, extraInfo);
    }
}
