package hotel_reservation.db.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;

public class ReservationFullDto {
    private final int id;
    private final int clientId;
    private final String clientName;
    private final Date reservationDate;
    private int roomId;
    private String roomTypeDescription;
    private BigDecimal roomRate;
    private Date startDate;
    private Date endDate;
    private Date cancellationDate;
    private String extraInfo;
    private BigDecimal invoice;

    public ReservationFullDto(int id,
                              int clientId,
                              String clientName,
                              int roomId,
                              String roomTypeDescription,
                              BigDecimal roomRate,
                              Date startDate,
                              Date endDate,
                              Date reservationDate,
                              Date cancellationDate,
                              String extraInfo) {
        this.id = id;
        this.clientId = clientId;
        this.clientName = clientName;
        this.roomId = roomId;
        this.roomTypeDescription = roomTypeDescription;
        this.roomRate = roomRate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.reservationDate = reservationDate;
        this.cancellationDate = cancellationDate;
        this.extraInfo = extraInfo;
        invoice = BigDecimal.valueOf((endDate.getTime() - startDate.getTime()) / (24 * 60 * 60 * 1000) * roomRate.longValue()).setScale(2);
    }

    public int getId() {
        return id;
    }

    public int getClientId() {
        return clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public int getRoomId() {
        return roomId;
    }

    public String getRoomTypeDescription() {
        return roomTypeDescription;
    }

    public BigDecimal getRoomRate() {
        return roomRate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Date getReservationDate() {
        return reservationDate;
    }

    public Date getCancellationDate() {
        return cancellationDate;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public BigDecimal getInvoice() {
        return invoice;
    }

    @Override
    public String toString() {
        return "ReservationFullDto{" +
                "id=" + id +
                ", clientId=" + clientId +
                ", clientName='" + clientName + '\'' +
                ", roomId=" + roomId +
                ", roomTypeDescription='" + roomTypeDescription + '\'' +
                ", roomRate=" + roomRate +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", reservationDate=" + reservationDate +
                ", cancellationDate=" + cancellationDate +
                ", extraInfo='" + extraInfo + '\'' +
                ", invoice=" + invoice +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationFullDto that = (ReservationFullDto) o;
        return id == that.id &&
                clientId == that.clientId &&
                roomId == that.roomId &&
                Objects.equals(clientName, that.clientName) &&
                Objects.equals(reservationDate, that.reservationDate) &&
                Objects.equals(roomTypeDescription, that.roomTypeDescription) &&
                Objects.equals(roomRate, that.roomRate) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(cancellationDate, that.cancellationDate) &&
                Objects.equals(extraInfo, that.extraInfo) &&
                Objects.equals(invoice, that.invoice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, clientName, reservationDate, roomId, roomTypeDescription, roomRate, startDate, endDate, cancellationDate, extraInfo, invoice);
    }
}
