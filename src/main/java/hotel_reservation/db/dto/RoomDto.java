package hotel_reservation.db.dto;

import java.util.Objects;

public class RoomDto {

    private final int number;
    private final int roomType;
    private final String info;

    public RoomDto(int number, int roomType, String info) {
        this.number = number;
        this.roomType = roomType;
        this.info = info;
    }

    static RoomDto convertFromCSV(String line) {
        int index1 = line.indexOf('"');
        int index2 = line.indexOf('"', index1 + 1);
        String lastPart = line.substring(index1 + 1, index2);   //last part is delimited by double-quote

        String[] parts = line.split("\\s*,\\s*");
        try {
            return new RoomDto(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), lastPart);
        } catch (Exception e) {
            System.out.println("failed to convert line to room: '" + line + "'" +
                    ", because: " + e);
            return null;
        }
    }

    public int getNumber() {
        return number;
    }

    public int getRoomType() {
        return roomType;
    }

    public String getInfo() {
        return info;
    }

    @Override
    public String toString() {
        return "RoomDto{" +
                "number=" + number +
                ", roomType=" + roomType +
                ", info='" + info + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomDto roomDto = (RoomDto) o;
        return number == roomDto.number &&
                roomType == roomDto.roomType &&
                Objects.equals(info, roomDto.info);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, roomType, info);
    }
}
