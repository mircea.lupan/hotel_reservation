package hotel_reservation.db.dto;

import hotel_reservation.db.dto.enums.RoomTypeDescription;

import java.math.BigDecimal;
import java.util.Objects;

public class RoomTypeDto {

    private final int id;
    private final RoomTypeDescription roomTypeDescription;
    private final BigDecimal rate;
    private final int capacity;

    public RoomTypeDto(int id, RoomTypeDescription roomTypeDescription, BigDecimal rate, int capacity) {
        this.id = id;
        this.roomTypeDescription = roomTypeDescription;
        this.rate = rate.setScale(2);
        this.capacity = capacity;
    }

    static RoomTypeDto convertFromCSV(String line) {
        String[] parts = line.split("\\s*,\\s*");   //remove whitespace and split by comma
        try {
            return new RoomTypeDto(Integer.parseInt(parts[0]),
                    RoomTypeDescription.valueOf(parts[1]),
                    BigDecimal.valueOf(Long.parseLong(parts[2])),
                    Integer.parseInt(parts[3]));
        } catch (Exception e) {
            System.out.println("failed to convert line to room type: '" + line + "'" +
                    ", because: " + e);
            return null;
        }
    }

    public int getId() {
        return id;
    }

    public RoomTypeDescription getRoomTypeDescription() {
        return roomTypeDescription;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public String toString() {
        return "RoomTypeDto{" +
                "id=" + id +
                ", roomTypeDescription='" + roomTypeDescription + '\'' +
                ", rate=" + rate +
                ", capacity=" + capacity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomTypeDto that = (RoomTypeDto) o;
        return id == that.id &&
                capacity == that.capacity &&
                roomTypeDescription == that.roomTypeDescription &&
                Objects.equals(rate, that.rate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomTypeDescription, rate, capacity);
    }

}
