package hotel_reservation.db.service;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.ClientDto;
import hotel_reservation.db.dto.ReservationDto;
import hotel_reservation.db.dto.RoomDto;
import hotel_reservation.db.dto.RoomTypeDto;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static hotel_reservation.db.dto.CsvUtils.*;

public class DbInitService {

    public static final String CREATE_ROOMTYPES_SQL =
            "CREATE TABLE IF NOT EXISTS ROOM_TYPES ( " +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "DESCRIPTION TEXT NOT NULL UNIQUE, " +
                    "RATE BIGDECIMAL NOT NULL, " +
                    "CAPACITY INTEGER NOT NULL" +
                    ");";

    public static final String CREATE_ROOMS_SQL =
            "CREATE TABLE IF NOT EXISTS ROOMS ( " +
                    "NUMBER INTEGER PRIMARY KEY UNIQUE, " +
                    "ROOM_TYPE_ID INTEGER REFERENCES ROOM_TYPES(ID), " +
                    "EXTRA_INFO TEXT" +
                    ");";

    public static final String CREATE_CLIENTS_SQL =
            "CREATE TABLE IF NOT EXISTS CLIENTS ( " +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "NAME TEXT NOT NULL, " +
                    "PHONE_NUMBER TEXT NOT NULL UNIQUE, " +
                    "EMAIL TEXT NOT NULL UNIQUE, " +
                    "ADDRESS TEXT NOT NULL " +
                    ");";

    public static final String CREATE_RESERVATIONS_SQL =
            "CREATE TABLE IF NOT EXISTS RESERVATIONS ( " +
                    "ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "CLIENT_ID INTEGER REFERENCES CLIENTS(ID), " +
                    "ROOM_ID INTEGER REFERENCES ROOMS(NUMBER), " +
                    "START_DATE DATETIME NOT NULL, " +
                    "END_DATE DATETIME NOT NULL, " +
                    "RESERVATION_DATE DATETIME NOT NULL, " +
                    "CANCELLATION_DATE DATETIME, " +
                    "EXTRA_INFO TEXT" +
                    ");";

    public static void createTablesAndInitialData() {
        createMissingTables();
        insertRoomTypesData();
        insertRoomsData();
        insertSomeClientData();
        insertSomeReservationData();
    }

    public static void createMissingTables() {
        try (Connection conn = DbManager.getConnection();
             Statement st = conn.createStatement()) {

            st.execute(CREATE_ROOMTYPES_SQL);
            st.execute(CREATE_ROOMS_SQL);
            st.execute(CREATE_CLIENTS_SQL);
            st.execute(CREATE_RESERVATIONS_SQL);

        } catch (SQLException e) {
            System.err.println("Error creating missing tables: " + e.getMessage());
        }
    }

    public static void insertRoomTypesData() {
        RoomTypeDao roomTypeDao = new RoomTypeDao();
        if (roomTypeDao.getAll().isEmpty()) {
            List<RoomTypeDto> roomTypes = loadRoomTypesFromCsvFile("src/main/resources/csv/room_types.csv");
            for (RoomTypeDto dto : roomTypes) {
                roomTypeDao.insert(dto);
            }
        }
    }

    public static void insertRoomsData() {
        RoomDao roomDao = new RoomDao();
        if (roomDao.getAll().isEmpty()) {
            List<RoomDto> rooms = loadRoomsFromCsvFile("src/main/resources/csv/rooms.csv");
            for (RoomDto dto : rooms) {
                roomDao.insert(dto);
            }
        }
    }

    public static void insertSomeClientData() {
        ClientDao clientDao = new ClientDao();
        if (clientDao.getAll().isEmpty()) {
            List<ClientDto> clients = loadClientsFromCsvFile("src/main/resources/csv/clients.csv");
            for (ClientDto dto : clients) {
                clientDao.insert(dto);
            }
        }
    }

    public static void insertSomeReservationData() {
        ReservationDao reservationDao = new ReservationDao();
        if (reservationDao.getAll().isEmpty()) {
            List<ReservationDto> reservations = loadReservationsFromCsvFile("src/main/resources/csv/reservations.csv");
            for (ReservationDto dto : reservations) {
                reservationDao.insert(dto);
            }
        }
    }

}
