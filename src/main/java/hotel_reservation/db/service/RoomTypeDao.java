package hotel_reservation.db.service;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.RoomTypeDto;
import hotel_reservation.db.dto.enums.RoomTypeDescription;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RoomTypeDao {

    /**
     * Load list of room types from DB (all found)
     */
    public List<RoomTypeDto> getAll() {

        String sql = "SELECT * " +
                "FROM ROOM_TYPES " +
                "ORDER BY ID";

        List<RoomTypeDto> types = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                types.add(new RoomTypeDto(
                        rs.getInt("ID"),
                        RoomTypeDescription.valueOf(rs.getString("DESCRIPTION")),
                        rs.getBigDecimal("RATE"),
                        rs.getInt("CAPACITY")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading all room types: " + e.getMessage());
        }
        return types;
    }

    /**
     * Load one specific room type from DB (by id)
     */
    public Optional<RoomTypeDto> getById(int id) {

        String sql = "SELECT * FROM ROOM_TYPES WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(new RoomTypeDto(
                            rs.getInt("ID"),
                            RoomTypeDescription.valueOf(rs.getString("DESCRIPTION")),
                            rs.getBigDecimal("RATE"),
                            rs.getInt("CAPACITY")));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading room type with id " + id + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    /**
     * Add a new room type to DB
     */
    public void insert(RoomTypeDto dto) {

        String sql = "INSERT INTO ROOM_TYPES " +
                "(DESCRIPTION, RATE, CAPACITY) " +
                "VALUES(?,?,?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, dto.getRoomTypeDescription().name());
            ps.setBigDecimal(2, dto.getRate());
            ps.setInt(3, dto.getCapacity());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed") ?
                    "a room type with same description already exists" :
                    e.getMessage(); //unknown error
            throw new RuntimeException("Error saving new room type: " + details);
        }
    }

    /**
     * Update an existing room type in DB
     */
    public void update(RoomTypeDto dto) {

        String sql = "UPDATE ROOM_TYPES " +
                "SET DESCRIPTION = ?, " +
                "RATE = ?, " +
                "CAPACITY = ? " +
                "WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement (based on dto data)
            ps.setString(1, dto.getRoomTypeDescription().name());
            ps.setBigDecimal(2, dto.getRate());
            ps.setInt(3, dto.getCapacity());
            ps.setInt(4, dto.getId());

            //execute it (no results to read after this one)
            ps.executeUpdate();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed") ?
                    "a room type with same description already exists" :
                    e.getMessage(); //unknown error
            throw new RuntimeException("Error while updating room type: " + details);
        }
    }

    /**
     * Delete a room type from DB (by id)
     */
    public void delete(int id) {

        String sql = "DELETE FROM ROOM_TYPES WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement
            ps.setInt(1, id);

            //execute it (no results to read after this one)
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while deleting room type " + id + ": " + e.getMessage());
        }
    }

}
