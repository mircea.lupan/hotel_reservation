package hotel_reservation.db.service;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.ReservationDto;
import hotel_reservation.db.dto.ReservationFullDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static hotel_reservation.ui.web.RoomsController.getTimeAtStartOfDay;

public class ReservationDao {

    /**
     * Load list of reservations from DB (all found)
     */
    public List<ReservationFullDto> getAll() {

        String sql = "SELECT r.*, rt.DESCRIPTION as ROOM_TYPE_DESCRIPTION, rt.RATE as ROOM_RATE, c.NAME as CLIENT_NAME " +
                "FROM RESERVATIONS r " +
                "JOIN ROOMS rm ON r.ROOM_ID = rm.NUMBER " +
                "JOIN ROOM_TYPES rt ON rm.ROOM_TYPE_ID = rt.ID " +
                "JOIN CLIENTS c ON r.CLIENT_ID = c.ID " +
                "ORDER BY START_DATE, END_DATE ";

        List<ReservationFullDto> reservations = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                reservations.add(new ReservationFullDto(
                        rs.getInt("ID"),
                        rs.getInt("CLIENT_ID"),
                        rs.getString("CLIENT_NAME"),
                        rs.getInt("ROOM_ID"),
                        rs.getString("ROOM_TYPE_DESCRIPTION"),
                        rs.getBigDecimal("ROOM_RATE"),
                        new Date(getTimeAtStartOfDay(rs.getDate("START_DATE").getTime())),
                        new Date(getTimeAtStartOfDay(rs.getDate("END_DATE").getTime())),
                        rs.getDate("RESERVATION_DATE"),
                        rs.getDate("CANCELLATION_DATE"),
                        rs.getString("EXTRA_INFO")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading all reservations: " + e.getMessage());
        }
        return reservations;
    }

    /**
     * Load one specific reservation from DB (by id)
     */
    public Optional<ReservationDto> get(int id) {

        String sql = "SELECT * FROM RESERVATIONS WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(new ReservationDto(
                            rs.getInt("ID"),
                            rs.getInt("CLIENT_ID"),
                            rs.getInt("ROOM_ID"),
                            rs.getDate("START_DATE"),
                            rs.getDate("END_DATE"),
                            rs.getDate("RESERVATION_DATE"),
                            rs.getDate("CANCELLATION_DATE"),
                            rs.getString("EXTRA_INFO")));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading reservation with id " + id + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    /**
     * Add a new reservation to DB
     */
    public void insert(ReservationDto dto) {

        String sql = "INSERT INTO RESERVATIONS " +
                "(CLIENT_ID, ROOM_ID, START_DATE, END_DATE, RESERVATION_DATE, CANCELLATION_DATE, EXTRA_INFO) " +
                "VALUES(?,?,?,?,?,?,?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, dto.getClientId());
            ps.setInt(2, dto.getRoomId());
            ps.setDate(3, dto.getStartDate());
            ps.setDate(4, dto.getEndDate());
            ps.setDate(5, dto.getReservationDate());
            ps.setDate(6, dto.getCancellationDate());
            ps.setString(7, dto.getExtraInfo());

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error inserting in db reservation " + dto + " : " + e.getMessage());
        }
    }

    /**
     * Update an existing reservation in DB
     */
    public void update(ReservationDto dto) {

        String sql = "UPDATE RESERVATIONS " +
                "SET CLIENT_ID = ?, " +
                "ROOM_ID = ?, " +
                "START_DATE = ?, " +
                "END_DATE = ?, " +
                "RESERVATION_DATE = ?, " +
                "CANCELLATION_DATE = ?, " +
                "EXTRA_INFO = ? " +
                "WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement (based on dto data)
            ps.setInt(1, dto.getClientId());
            ps.setInt(2, dto.getRoomId());
            ps.setDate(3, dto.getStartDate());
            ps.setDate(4, dto.getEndDate());
            ps.setDate(5, dto.getReservationDate());
            ps.setDate(6, dto.getCancellationDate());
            ps.setString(7, dto.getExtraInfo());
            ps.setInt(8, dto.getId());

            //execute it (no results to read after this one)
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error while updating reservation " + dto + " : " + e.getMessage());
        }
    }

    /**
     * Delete a reservation from DB (by id)
     */
    public void delete(int id) {

        String sql = "DELETE FROM RESERVATIONS WHERE ID = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement
            ps.setInt(1, id);

            //execute it (no results to read after this one)
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while deleting reservation " + id + ": " + e.getMessage());
        }
    }

    /**
     * Cancel a reservation from DB (by id)
     */
    public void cancel(int id) {

        String sql = "UPDATE RESERVATIONS " +
                "SET CANCELLATION_DATE = ? " +
                "WHERE ID = ? ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement
            ps.setDate(1, new Date(System.currentTimeMillis()));
            ps.setInt(2, id);

            //execute it (no results to read after this one)
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error while canceling reservation " + id + ": " + e.getMessage());
        }
    }

}
