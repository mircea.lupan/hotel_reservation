package hotel_reservation.db.service;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.RoomDto;
import hotel_reservation.db.dto.RoomFullDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static hotel_reservation.ui.web.RoomsController.getTimeAtStartOfDay;

public class RoomDao {

    /**
     * Load list of rooms from DB (all found)
     */
    public List<RoomFullDto> getAll() {

        String sql = "SELECT r.*, rt.DESCRIPTION AS ROOM_TYPE_DESCRIPTION, rt.RATE AS ROOM_RATE, rv.START_DATE, rv.END_DATE " +
                "FROM ROOMS r " +
                "JOIN ROOM_TYPES rt ON r.ROOM_TYPE_ID = rt.ID " +
                "LEFT JOIN RESERVATIONS rv ON r.NUMBER = rv.ROOM_ID " +
                "WHERE rv.CANCELLATION_DATE ISNULL " +
                "ORDER BY r.NUMBER, rv.START_DATE ";

        List<RoomFullDto> rooms = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                rooms.add(new RoomFullDto(
                        -1,
                        rs.getInt("NUMBER"),
                        rs.getInt("ROOM_TYPE_ID"),
                        rs.getString("ROOM_TYPE_DESCRIPTION"),
                        rs.getBigDecimal("ROOM_RATE"),
                        rs.getString("EXTRA_INFO"),
                        (rs.getDate("START_DATE") != null)
                                ? new Date(getTimeAtStartOfDay(rs.getDate("START_DATE").getTime()))
                                : null,
                        (rs.getDate("END_DATE") != null)
                                ? new Date(getTimeAtStartOfDay(rs.getDate("END_DATE").getTime()))
                                : null)
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading all rooms: " + e.getMessage());
        }
        return rooms;
    }

    /**
     * Load one specific room from DB (by number)
     */
    public Optional<RoomDto> get(int number) {

        String sql = "SELECT * FROM ROOMS WHERE NUMBER = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, number);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(new RoomDto(
                            rs.getInt("NUMBER"),
                            rs.getInt("ROOM_TYPE_ID"),
                            rs.getString("EXTRA_INFO")));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading room with number " + number + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    /**
     * Add a new room to DB
     */
    public void insert(RoomDto dto) {

        String sql = "INSERT INTO ROOMS " +
                "(NUMBER, ROOM_TYPE_ID, EXTRA_INFO) " +
                "VALUES(?,?,?)";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, dto.getNumber());
            ps.setInt(2, dto.getRoomType());
            ps.setString(3, dto.getInfo());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed") ?
                    "a room with same number already exists" :
                    e.getMessage(); //unknown error
            throw new RuntimeException("Error inserting in db room " + dto + " : " + details);
        }
    }

    /**
     * Update an existing room in DB
     */
    public void update(RoomDto dto) {

        String sql = "UPDATE ROOMS " +
                "SET ROOM_TYPE_ID = ?, " +
                "EXTRA_INFO = ? " +
                "WHERE NUMBER = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement (based on dto data)
            ps.setInt(1, dto.getRoomType());
            ps.setString(2, dto.getInfo());
            ps.setInt(3, dto.getNumber());

            //execute it (no results to read after this one)
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error while updating room " + dto + " : " + e.getMessage());
        }
    }

    /**
     * Delete a room from DB (by number)
     */
    public void delete(int number) {

        String sql = "DELETE FROM ROOMS WHERE NUMBER = ?";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement
            ps.setInt(1, number);

            //execute it (no results to read after this one)
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while deleting room " + number + ": " + e.getMessage());
        }
    }

}
