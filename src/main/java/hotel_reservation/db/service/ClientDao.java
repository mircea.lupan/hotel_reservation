package hotel_reservation.db.service;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.ClientDto;
import hotel_reservation.db.dto.ClientFullDto;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static hotel_reservation.ui.web.RoomsController.getTimeAtStartOfDay;

public class ClientDao {

    /**
     * Load list of clients from DB (all found)
     */
    public List<ClientFullDto> getFullAll() {

        String sql = "SELECT c.*, rv.START_DATE, rv.END_DATE " +
                "FROM CLIENTS c " +
                "LEFT JOIN RESERVATIONS rv ON c.ID = rv.CLIENT_ID " +
                "WHERE rv.CANCELLATION_DATE ISNULL " +
                "ORDER BY NAME ";

        List<ClientFullDto> clients = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                clients.add(new ClientFullDto(
                        rs.getInt("ID"),
                        rs.getString("NAME"),
                        rs.getString("PHONE_NUMBER"),
                        rs.getString("EMAIL"),
                        rs.getString("ADDRESS"),
                        (rs.getDate("START_DATE") != null)
                                ? new Date(getTimeAtStartOfDay(rs.getDate("START_DATE").getTime()))
                                : null,
                        (rs.getDate("END_DATE") != null)
                                ? new Date(getTimeAtStartOfDay(rs.getDate("END_DATE").getTime()))
                                : null)
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading all clients: " + e.getMessage());
        }
        return clients;
    }

    /**
     * Load list of clients from DB and their's last reservation
     */
    public List<ClientFullDto> getAllLast() {

        String sql = "SELECT c.*, MAX(rv.START_DATE) AS START_DATE, rv.END_DATE " +
                "FROM CLIENTS c, RESERVATIONS rv " +
                "WHERE c.ID = rv.CLIENT_ID " +
                "GROUP BY c.ID " +
                "ORDER BY NAME ";

        List<ClientFullDto> clients = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                clients.add(new ClientFullDto(
                        rs.getInt("ID"),
                        rs.getString("NAME"),
                        rs.getString("PHONE_NUMBER"),
                        rs.getString("EMAIL"),
                        rs.getString("ADDRESS"),
                        (rs.getDate("START_DATE") != null)
                                ? new Date(getTimeAtStartOfDay(rs.getDate("START_DATE").getTime()))
                                : null,
                        (rs.getDate("END_DATE") != null)
                                ? new Date(getTimeAtStartOfDay(rs.getDate("END_DATE").getTime()))
                                : null)
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading all clients: " + e.getMessage());
        }
        return clients;
    }

    /**
     * Load list of clients from DB (all found)
     */
    public List<ClientDto> getAll() {

        String sql = "SELECT * " +
                "FROM CLIENTS " +
                "ORDER BY NAME ";

        List<ClientDto> clients = new ArrayList<>();

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                clients.add(new ClientDto(
                        rs.getInt("ID"),
                        rs.getString("NAME"),
                        rs.getString("PHONE_NUMBER"),
                        rs.getString("EMAIL"),
                        rs.getString("ADDRESS")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading all clients: " + e.getMessage());
        }
        return clients;
    }

    /**
     * Load one specific client from DB (by id)
     */
    public Optional<ClientDto> get(int id) {

        String sql = "SELECT * FROM CLIENTS WHERE ID = ? ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setInt(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(new ClientDto(
                            rs.getInt("ID"),
                            rs.getString("NAME"),
                            rs.getString("PHONE_NUMBER"),
                            rs.getString("EMAIL"),
                            rs.getString("ADDRESS")));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading client " + id + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    /**
     * Load one specific client from DB (by email)
     */
    public Optional<ClientDto> getByEmail(String email) {

        String sql = "SELECT * FROM CLIENTS WHERE EMAIL = ? ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, email);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(
                            new ClientDto(
                                    rs.getInt("ID"),
                                    rs.getString("NAME"),
                                    rs.getString("PHONE_NUMBER"),
                                    rs.getString("EMAIL"),
                                    rs.getString("ADDRESS")));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Error loading client client with email " + email + " : " + e.getMessage());
        }
        return Optional.empty();
    }

    /**
     * Add a new client to DB
     */
    public void insert(ClientDto dto) {

        String sql = "INSERT INTO CLIENTS " +
                "(NAME, PHONE_NUMBER, EMAIL, ADDRESS) " +
                "VALUES(?,?,?,?) ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, dto.getName());
            ps.setString(2, dto.getPhoneNumber());
            ps.setString(3, dto.getEmail());
            ps.setString(4, dto.getAddress());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed") ?
                    "a client with same email or phone number already exists" :
                    e.getMessage(); //unknown error
            throw new RuntimeException("Error saving new client: " + details);
        }
    }

    /**
     * Update an existing client in DB
     */
    public void update(ClientDto dto) {

        String sql = "UPDATE CLIENTS " +
                "SET NAME = ?, " +
                "PHONE_NUMBER = ?, " +
                "EMAIL = ?, " +
                "ADDRESS = ? " +
                "WHERE ID = ? ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement (based on dto data)
            ps.setString(1, dto.getName());
            ps.setString(2, dto.getPhoneNumber());
            ps.setString(3, dto.getEmail());
            ps.setString(4, dto.getAddress());
            ps.setInt(5, dto.getId());

            //execute it (no results to read after this one)
            ps.executeUpdate();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed") ?
                    "a client with same email or phone number already exists" :
                    e.getMessage(); //unknown error
            throw new RuntimeException("Error while updating client " + dto + " : " + details);
        }
    }

    /**
     * Delete a client from DB (by id)
     */
    public void delete(int id) {

        String sql = "DELETE FROM CLIENTS WHERE ID = ? ";

        try (Connection conn = DbManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement
            ps.setInt(1, id);

            //execute it (no results to read after this one)
            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while deleting client " + id + " : " + e.getMessage());
        }
    }

}
