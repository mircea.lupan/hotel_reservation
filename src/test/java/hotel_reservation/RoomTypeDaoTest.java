package hotel_reservation;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.RoomTypeDto;
import hotel_reservation.db.dto.enums.RoomTypeDescription;
import hotel_reservation.db.service.DbInitService;
import hotel_reservation.db.service.RoomTypeDao;
import org.junit.*;

import java.io.File;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RoomTypeDaoTest {

    private static final String TEST_DB_FILE = "hotel_reservation_test.db";

    private static final List<RoomTypeDto> roomTypesList = Arrays.asList(
            new RoomTypeDto(-1, RoomTypeDescription.SINGLE, new BigDecimal("100.00"), 1),
            new RoomTypeDto(-1, RoomTypeDescription.DOUBLE, new BigDecimal("150.00"), 2),
            new RoomTypeDto(-1, RoomTypeDescription.TWIN, new BigDecimal("170.00"), 2),
            new RoomTypeDto(-1, RoomTypeDescription.TRIPLE, new BigDecimal("200.00"), 3));

    private final RoomTypeDao dao = new RoomTypeDao();

    /**
     * This will be called only once before running the tests, due to @BeforeClass
     */
    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE); //use a separate db for test, to avoid overwriting the real one
        DbInitService.createMissingTables();
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    /**
     * This will be called once before each test! due to @Before
     */
    @Before
    public void insertRowsBeforeTest() {
        assertTrue(dao.getAll().isEmpty());
        for (RoomTypeDto dto : roomTypesList) {
            dao.insert(dto);
        }
    }

    @After
    public void deleteRowsAfterTest() {
        dao.getAll().forEach(dto -> dao.delete(dto.getId()));
        assertTrue(dao.getAll().isEmpty());
    }

    @Test
    public void getAll() {
        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void get() {
        RoomTypeDto item1fromDb = dao.getAll().get(0);
        assertEqualItemsExceptId(roomTypesList.get(0), dao.getById(item1fromDb.getId()).get());
    }

    @Test
    public void get_forInvalidId() {
        assertFalse(dao.getById(-99).isPresent());
    }

    @Test
    public void insert() {
        assertEquals(4, dao.getAll().size());

        RoomTypeDto newItem = new RoomTypeDto(-1, RoomTypeDescription.QUAD, new BigDecimal("250.00"), 4);
        dao.insert(newItem);

        assertTrue(dao.getAll().stream().anyMatch(dto -> isEqualItemsExceptId(dto, newItem)));
    }

    @Test
    public void insert_descriptionMustBeUnique() {
        assertEquals(4, dao.getAll().size());

        RoomTypeDto newItem = new RoomTypeDto(-1, RoomTypeDescription.SINGLE, new BigDecimal("100.00"), 1);

        //insert should fail, with an exception with specific message
        try {
            dao.insert(newItem);
            Assert.fail("insert room type with duplicate description should throw exception");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage().contains("a room type with same description already exists"));
        }

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void update() {
        RoomTypeDto dbItem = dao.getAll().get(0);
        RoomTypeDto updatedItem = new RoomTypeDto(dbItem.getId(), RoomTypeDescription.SINGLE, new BigDecimal("122.00"), 1);
        dao.update(updatedItem);
        assertEqualItemsExceptId(updatedItem, dao.getById(dbItem.getId()).get());
    }

    @Test
    public void update_forInvalidId() {
        dao.update(new RoomTypeDto(-22, RoomTypeDescription.SINGLE, new BigDecimal("122.00"), 1));
        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void update_forDuplicatedDescription() {
        RoomTypeDto dbItem = dao.getAll().get(0);

        RoomTypeDto item = new RoomTypeDto(dbItem.getId(), roomTypesList.get(1).getRoomTypeDescription(), dbItem.getRate(), dbItem.getCapacity());

        //insert should fail, with an exception with specific message
        try {
            dao.update(item);
            Assert.fail("insert room type with duplicate description should throw exception");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage().contains("a room type with same description already exists"));
        }

        assertTrue(isEqualItemsExceptId(dbItem, dao.getById(dbItem.getId()).get()));
    }

    @Test
    public void delete() {
        List<RoomTypeDto> itemsFromDb = dao.getAll();
        dao.delete(itemsFromDb.get(0).getId());

        List<RoomTypeDto> itemsInDBAfterDelete = dao.getAll();
        assertEquals(3, itemsInDBAfterDelete.size());
        assertEqualItemsExceptId(itemsFromDb.get(1), itemsInDBAfterDelete.get(0));
        assertEqualItemsExceptId(itemsFromDb.get(2), itemsInDBAfterDelete.get(1));
        assertEqualItemsExceptId(itemsFromDb.get(3), itemsInDBAfterDelete.get(2));
    }

    @Test
    public void delete_forInvalidId() {
        checkOnlyTheSampleItemsArePresentInDb();
        dao.delete(-66);
        checkOnlyTheSampleItemsArePresentInDb();
    }

    private void checkOnlyTheSampleItemsArePresentInDb() {
        List<RoomTypeDto> itemsFromDb = dao.getAll();
        assertEquals(4, itemsFromDb.size());
        assertEqualItemsExceptId(roomTypesList.get(0), itemsFromDb.get(0));
        assertEqualItemsExceptId(roomTypesList.get(1), itemsFromDb.get(1));
        assertEqualItemsExceptId(roomTypesList.get(2), itemsFromDb.get(2));
        assertEqualItemsExceptId(roomTypesList.get(3), itemsFromDb.get(3));
    }

    private void assertEqualItemsExceptId(RoomTypeDto item1, RoomTypeDto item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                isEqualItemsExceptId(item1, item2));
    }

    private boolean isEqualItemsExceptId(RoomTypeDto item1, RoomTypeDto item2) {
        return (item1.getRoomTypeDescription() == item2.getRoomTypeDescription()) &&
                item1.getRate().equals(item2.getRate()) &&
                (item1.getCapacity() == item2.getCapacity());
    }
}
