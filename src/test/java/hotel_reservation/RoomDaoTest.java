package hotel_reservation;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.RoomDto;
import hotel_reservation.db.dto.RoomFullDto;
import hotel_reservation.db.service.DbInitService;
import hotel_reservation.db.service.RoomDao;
import org.junit.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class RoomDaoTest {

    private static final String TEST_DB_FILE = "hotel_reservation_test.db";

    private static final List<RoomDto> roomsList = Arrays.asList(
            new RoomDto(1, 1, ""),
            new RoomDto(2, 2, ""),
            new RoomDto(3, 3, ""),
            new RoomDto(4, 4, ""));

    private final RoomDao dao = new RoomDao();

    /**
     * This will be called only once before running the tests, due to @BeforeClass
     */
    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE); //use a separate db for test, to avoid overwriting the real one
        DbInitService.createMissingTables();
        DbInitService.insertRoomTypesData();
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    /**
     * This will be called once before each test! due to @Before
     */
    @Before
    public void insertRowsBeforeTest() {
        assertTrue(dao.getAll().isEmpty());
        for (RoomDto dto : roomsList) {
            dao.insert(dto);
        }
    }

    @After
    public void deleteRowsAfterTest() {
        dao.getAll().forEach(dto -> dao.delete(dto.getNumber()));
        assertTrue(dao.getAll().isEmpty());
    }

    @Test
    public void getAll() {
        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void get() {
        RoomFullDto item1fromDb = dao.getAll().get(0);
        assertEqualItems(roomsList.get(0), dao.get(item1fromDb.getNumber()).get());
    }

    @Test
    public void get_forInvalidId() {
        assertFalse(dao.get(-99).isPresent());
    }

    @Test
    public void insert() {
        assertEquals(4, dao.getAll().size());

        RoomDto newItem = new RoomDto(5, 5, "");
        dao.insert(newItem);

        assertTrue(dao.getAll().stream().anyMatch(dto -> isEqualItems(newItem,
                new RoomDto(dto.getNumber(), dto.getRoomType(), dto.getInfo()))));
    }

    @Test
    public void insert_numberMustBeUnique() {
        assertEquals(4, dao.getAll().size());

        RoomDto newItem = new RoomDto(1, 5, "");

        //insert should fail, with an exception with specific message
        try {
            dao.insert(newItem);
            Assert.fail("insert room with duplicate number should throw exception");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage().contains("a room with same number already exists"));
        }

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void update() {
        RoomFullDto dbItem = dao.getAll().get(0);
        RoomDto updatedItem = new RoomDto(dbItem.getNumber(), dbItem.getRoomType(), "abc");
        dao.update(updatedItem);
        assertEqualItems(updatedItem, dao.get(dbItem.getNumber()).get());
    }

    @Test
    public void update_forInvalidNumber() {
        dao.update(new RoomDto(-22, 5, ""));
        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void delete() {
        List<RoomFullDto> itemsFromDb = dao.getAll();
        dao.delete(itemsFromDb.get(0).getNumber());

        List<RoomFullDto> itemsInDBAfterDelete = dao.getAll();
        assertEquals(3, itemsInDBAfterDelete.size());
        assertEqualItems(itemsFromDb.get(1), itemsInDBAfterDelete.get(0));
        assertEqualItems(itemsFromDb.get(2), itemsInDBAfterDelete.get(1));
        assertEqualItems(itemsFromDb.get(3), itemsInDBAfterDelete.get(2));
    }

    @Test
    public void delete_forInvalidId() {
        checkOnlyTheSampleItemsArePresentInDb();
        dao.delete(-66);
        checkOnlyTheSampleItemsArePresentInDb();
    }

    private void checkOnlyTheSampleItemsArePresentInDb() {
        List<RoomFullDto> itemsFromDb = dao.getAll();
        assertEquals(4, itemsFromDb.size());
        assertEqualItems(roomsList.get(0), itemsFromDb.get(0));
        assertEqualItems(roomsList.get(1), itemsFromDb.get(1));
        assertEqualItems(roomsList.get(2), itemsFromDb.get(2));
        assertEqualItems(roomsList.get(3), itemsFromDb.get(3));
    }

    private void assertEqualItems(RoomFullDto item1, RoomFullDto item2) {
        assertTrue("items should be equal: " + item1 + ", " + item2,
                isEqualItems(new RoomDto(item1.getNumber(), item1.getRoomType(), item1.getInfo()),
                        new RoomDto(item2.getNumber(), item2.getRoomType(), item2.getInfo())));
    }

    private void assertEqualItems(RoomDto item1, RoomFullDto item2) {
        assertTrue("items should be equal: " + item1 + ", " + item2,
                isEqualItems(item1, new RoomDto(item2.getNumber(), item2.getRoomType(), item2.getInfo())));
    }

    private void assertEqualItems(RoomDto item1, RoomDto item2) {
        assertTrue("items should be equal: " + item1 + ", " + item2,
                isEqualItems(item1, item2));
    }

    private boolean isEqualItems(RoomDto item1, RoomDto item2) {
        return (item1.getNumber() == item2.getNumber()) &&
                (item1.getRoomType() == item2.getRoomType()) &&
                (item1.getInfo().equals(item2.getInfo()));
    }

}
