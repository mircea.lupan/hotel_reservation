package hotel_reservation;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.ClientDto;
import hotel_reservation.db.service.ClientDao;
import hotel_reservation.db.service.DbInitService;
import org.junit.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ClientDaoTest {

    private static final String TEST_DB_FILE = "hotel_reservation_test.db";

    private static final List<ClientDto> clientsList = Arrays.asList(
            new ClientDto(-1, "Client 1", "0740101010", "client1@xyz.com", "Iasi"),
            new ClientDto(-1, "Client 2", "0750202020", "client2@xyz.com", "Bucuresti"),
            new ClientDto(-1, "Client 3", "0760303030", "client3@xyz.com", "Constanta"));

    private final ClientDao dao = new ClientDao();

    /**
     * This will be called only once before running the tests, due to @BeforeClass
     */
    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE); //use a separate db for test, to avoid overwriting the real one
        DbInitService.createMissingTables();
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    /**
     * This will be called once before each test! due to @Before
     */
    @Before
    public void insertRowsBeforeTest() {
        assertTrue(dao.getAll().isEmpty());
        for (ClientDto dto : clientsList) {
            dao.insert(dto);
        }
    }

    @After
    public void deleteRowsAfterTest() {
        dao.getAll().forEach(dto -> dao.delete(dto.getId()));
        assertTrue(dao.getAll().isEmpty());
    }

    @Test
    public void getAll() {
        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void get() {
        ClientDto item1fromDb = dao.getAll().get(0);
        assertEqualItemsExceptId(clientsList.get(0), dao.get(item1fromDb.getId()).get());
    }

    @Test
    public void get_forInvalidId() {
        assertFalse(dao.get(-99).isPresent());
    }

    @Test
    public void insert() {
        assertEquals(3, dao.getAll().size());

        ClientDto newItem = new ClientDto(-1, "Client 4", "0770404040", "client4@xyz.com", "Iasi");
        dao.insert(newItem);

        ClientDto item = dao.getByEmail(newItem.getEmail()).get();
        assertEqualItemsExceptId(newItem, item);
    }

    @Test
    public void insert_emailMustBeUnique() {
        assertEquals(3, dao.getAll().size());
        ClientDto existing = clientsList.get(0);

        ClientDto newItem = new ClientDto(-1, "Client 4", "0770404040", existing.getEmail(), "Iasi");

        //insert should fail, with an exception with specific message
        try {
            dao.insert(newItem);
            Assert.fail("insert client with duplicate email should throw exception");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage().contains("a client with same email or phone number already exists"));
        }

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void insert_phoneNumberMustBeUnique() {
        assertEquals(3, dao.getAll().size());
        ClientDto existing = clientsList.get(0);

        ClientDto newItem = new ClientDto(-1, "Client 4", existing.getPhoneNumber(), "client@xyz.com", "Iasi");

        //insert should fail, with an exception with specific message
        try {
            dao.insert(newItem);
            Assert.fail("insert client with duplicate phone number should throw exception");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage().contains("a client with same email or phone number already exists"));
        }

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void update() {
        ClientDto dbItem = dao.getAll().get(0);
        ClientDto updatedItem = new ClientDto(dbItem.getId(), "abc", dbItem.getPhoneNumber(), dbItem.getEmail(), dbItem.getAddress());
        dao.update(updatedItem);
        assertEqualItemsExceptId(updatedItem, dao.getByEmail(dbItem.getEmail()).get());
    }

    @Test
    public void update_forInvalidId() {
        ClientDto dbItem = dao.getAll().get(0);
        dao.update(new ClientDto(-22, "abc", dbItem.getPhoneNumber(), dbItem.getEmail(), dbItem.getAddress()));
        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void update_forDuplicatedEmail() {
        ClientDto dbItem = dao.getAll().get(0);

        try {
            dao.update(new ClientDto(dbItem.getId(), dbItem.getName(), dbItem.getPhoneNumber(), clientsList.get(1).getEmail(), dbItem.getAddress())); //should fail, but with no exception thrown
            Assert.fail("client with duplicate email should throw exception");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage().contains("a client with same email or phone number already exists"));
        }

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void update_forDuplicatedPhoneNumber() {
        ClientDto dbItem = dao.getAll().get(0);

        try {
            dao.update(new ClientDto(dbItem.getId(), dbItem.getName(), dbItem.getPhoneNumber(), clientsList.get(1).getEmail(), dbItem.getAddress())); //should fail, but with no exception thrown
            Assert.fail("client with duplicate phone number should throw exception");
        } catch (RuntimeException e) {
            assertTrue(e.getMessage().contains("a client with same email or phone number already exists"));
        }

        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void delete() {
        List<ClientDto> itemsFromDb = dao.getAll();
        dao.delete(itemsFromDb.get(0).getId());

        List<ClientDto> itemsInDBAfterDelete = dao.getAll();
        assertEquals(2, itemsInDBAfterDelete.size());
        assertEqualItemsExceptId(itemsFromDb.get(1), itemsInDBAfterDelete.get(0));
        assertEqualItemsExceptId(itemsFromDb.get(2), itemsInDBAfterDelete.get(1));
    }

    @Test
    public void delete_forInvalidId() {
        checkOnlyTheSampleItemsArePresentInDb();
        dao.delete(-66);
        checkOnlyTheSampleItemsArePresentInDb();
    }

    private void checkOnlyTheSampleItemsArePresentInDb() {
        List<ClientDto> itemsFromDb = dao.getAll();
        assertEquals(3, itemsFromDb.size());
        assertEqualItemsExceptId(clientsList.get(0), itemsFromDb.get(0));
        assertEqualItemsExceptId(clientsList.get(1), itemsFromDb.get(1));
        assertEqualItemsExceptId(clientsList.get(2), itemsFromDb.get(2));
    }

    private void assertEqualItemsExceptId(ClientDto item1, ClientDto item2) {
        assertTrue("items should be equal (except id): " + item1 + ", " + item2,
                (item1.getName().equals(item2.getName())) &&
                        (item1.getEmail().equals(item2.getEmail())) &&
                        (item1.getAddress().equals(item2.getAddress())));
    }
}
