package hotel_reservation;

import hotel_reservation.db.DbManager;
import hotel_reservation.db.dto.ClientDto;
import hotel_reservation.db.dto.ReservationDto;
import hotel_reservation.db.dto.ReservationFullDto;
import hotel_reservation.db.dto.RoomDto;
import hotel_reservation.db.service.ClientDao;
import hotel_reservation.db.service.DbInitService;
import hotel_reservation.db.service.ReservationDao;
import hotel_reservation.db.service.RoomDao;
import org.junit.*;

import java.io.File;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static hotel_reservation.ui.web.RoomsController.getTimeAtStartOfDay;
import static org.junit.Assert.*;

public class ReservationDaoTest {

    private static final String TEST_DB_FILE = "hotel_reservation_test.db";

    private static final long baseTime = getTimeAtStartOfDay(System.currentTimeMillis());

    private static final List<RoomDto> roomsList = Arrays.asList(
            new RoomDto(1, 1, ""),
            new RoomDto(2, 2, ""),
            new RoomDto(3, 3, ""),
            new RoomDto(4, 4, ""));

    private static final List<ClientDto> clientsList = Arrays.asList(
            new ClientDto(-1, "Client 1", "0740101010", "client1@xyz.com", "Iasi"),
            new ClientDto(-1, "Client 2", "0750202020", "client2@xyz.com", "Bucuresti"),
            new ClientDto(-1, "Client 3", "0760303030", "client3@xyz.com", "Constanta"));
    private static final RoomDao roomDao = new RoomDao();
    private static final ClientDao clientDao = new ClientDao();
    private final List<ReservationDto> reservationsList = Arrays.asList(
            new ReservationDto(-1, 1, 2,
                    new Date(baseTime + 5 * 86400000), new Date(baseTime + 8 * 86400000), new Date(baseTime), null,
                    ""),
            new ReservationDto(-1, 2, 3,
                    new Date(baseTime + 10 * 86400000), new Date(baseTime + 14 * 86400000), new Date(baseTime + 1000), null,
                    ""),
            new ReservationDto(-1, 3, 1,
                    new Date(baseTime + 3 * 86400000), new Date(baseTime + 6 * 86400000), new Date(baseTime + 5000), new Date(baseTime + 9000),
                    ""),
            new ReservationDto(-1, 3, 1,
                    new Date(baseTime + 20L * 86400000), new Date(baseTime + 24L * 86400000), new Date(baseTime + 6000), null,
                    ""));
    private final ReservationDao dao = new ReservationDao();

    /**
     * This will be called only once before running the tests, due to @BeforeClass
     */
    @BeforeClass
    public static void initDbBeforeAnyTests() {
        DbManager.setDbFile(TEST_DB_FILE); //use a separate db for test, to avoid overwriting the real one
        DbInitService.createMissingTables();
        DbInitService.insertRoomTypesData();
        for (RoomDto dto : roomsList) {
            roomDao.insert(dto);
        }
        for (ClientDto dto : clientsList) {
            clientDao.insert(dto);
        }
    }

    @AfterClass
    public static void deleteDbFileAfterAllTests() {
        new File(TEST_DB_FILE).delete();
    }

    /**
     * This will be called once before each test! due to @Before
     */
    @Before
    public void insertRowsBeforeTest() {
        assertTrue(dao.getAll().isEmpty());
        for (ReservationDto dto : reservationsList) {
            dao.insert(dto);
        }
    }

    @After
    public void deleteRowsAfterTest() {
        dao.getAll().forEach(dto -> dao.delete(dto.getId()));
        assertTrue(dao.getAll().isEmpty());
    }

    @Test
    public void getAll() {
        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void get() {
        ReservationFullDto item1FromDb = dao.getAll().get(0);
        assertEqualItemsExceptId(reservationsList.get(2), dao.get(item1FromDb.getId()).get());
    }

    @Test
    public void get_forInvalidId() {
        assertFalse(dao.get(-99).isPresent());
    }

    @Test
    public void insert() {
        assertEquals(4, dao.getAll().size());

        ReservationDto newItem = new ReservationDto(-1, 1, 1,
                new Date(baseTime + 30L * 86400000), new Date(baseTime + 35L * 86400000), new Date(baseTime + 10000), null,
                "");
        dao.insert(newItem);

        assertTrue(dao.getAll().stream().anyMatch(dto -> isEqualItemsExceptId(newItem, dto)));
    }

    @Test
    public void update() {
        ReservationFullDto dbItem = dao.getAll().get(0);
        ReservationDto updatedItem = new ReservationDto(dbItem.getId(), 1, 1,
                new Date(baseTime + 30L * 86400000), new Date(baseTime + 35L * 86400000), new Date(baseTime + 10000), null,
                "abc");
        dao.update(updatedItem);
        assertEqualItemsExceptId(updatedItem, dao.get(dbItem.getId()).get());
    }

    @Test
    public void update_forInvalidId() {
        ReservationFullDto dbItem = dao.getAll().get(0);
        dao.update(new ReservationDto(-1, 1, 1,
                dbItem.getStartDate(), dbItem.getEndDate(), dbItem.getReservationDate(), dbItem.getCancellationDate(),
                ""));
        checkOnlyTheSampleItemsArePresentInDb();
    }

    @Test
    public void delete() {
        List<ReservationFullDto> itemsFromDb = dao.getAll();
        dao.delete(itemsFromDb.get(0).getId());

        List<ReservationFullDto> itemsInDBAfterDelete = dao.getAll();
        assertEquals(3, itemsInDBAfterDelete.size());
        assertEqualItemsExceptId(itemsFromDb.get(1), itemsInDBAfterDelete.get(0));
        assertEqualItemsExceptId(itemsFromDb.get(2), itemsInDBAfterDelete.get(1));
        assertEqualItemsExceptId(itemsFromDb.get(3), itemsInDBAfterDelete.get(2));
    }

    @Test
    public void delete_forInvalidId() {
        checkOnlyTheSampleItemsArePresentInDb();
        dao.delete(-66);
        checkOnlyTheSampleItemsArePresentInDb();
    }

    private void checkOnlyTheSampleItemsArePresentInDb() {
        List<ReservationFullDto> itemsFromDb = dao.getAll();
        assertEquals(4, itemsFromDb.size());
        assertEqualItemsExceptId(reservationsList.get(0), itemsFromDb.get(1));
        assertEqualItemsExceptId(reservationsList.get(1), itemsFromDb.get(2));
        assertEqualItemsExceptId(reservationsList.get(2), itemsFromDb.get(0));
        assertEqualItemsExceptId(reservationsList.get(3), itemsFromDb.get(3));
    }

    private void assertEqualItemsExceptId(ReservationFullDto item1, ReservationFullDto item2) {
        assertTrue("items should be equal: " + item1 + ", " + item2,
                isEqualItemsExceptId(item1, item2));
    }

    private boolean isEqualItemsExceptId(ReservationFullDto item1, ReservationFullDto item2) {
        return (item1.getClientId() == item2.getClientId()) &&
                (item1.getRoomId() == item2.getRoomId()) &&
                (item1.getStartDate().equals(item2.getStartDate())) &&
                (item1.getEndDate().equals(item2.getEndDate())) &&
                (item1.getReservationDate().equals(item2.getReservationDate())) &&
                (Objects.equals(item1.getCancellationDate(), item2.getCancellationDate())) &&
                (item1.getExtraInfo().equals(item2.getExtraInfo()));
    }

    private void assertEqualItemsExceptId(ReservationDto item1, ReservationFullDto item2) {
        assertTrue("items should be equal: " + item1 + ", " + item2,
                isEqualItemsExceptId(item1, item2));
    }

    private boolean isEqualItemsExceptId(ReservationDto item1, ReservationFullDto item2) {
        return (item1.getClientId() == item2.getClientId()) &&
                (item1.getRoomId() == item2.getRoomId()) &&
                (item1.getStartDate().equals(item2.getStartDate())) &&
                (item1.getEndDate().equals(item2.getEndDate())) &&
                (item1.getReservationDate().equals(item2.getReservationDate())) &&
                (Objects.equals(item1.getCancellationDate(), item2.getCancellationDate())) &&
                (item1.getExtraInfo().equals(item2.getExtraInfo()));
    }

    private void assertEqualItemsExceptId(ReservationDto item1, ReservationDto item2) {
        assertTrue("items should be equal: " + item1 + ", " + item2,
                isEqualItemsExceptId(item1, item2));
    }

    private boolean isEqualItemsExceptId(ReservationDto item1, ReservationDto item2) {
        return (item1.getClientId() == item2.getClientId()) &&
                (item1.getRoomId() == item2.getRoomId()) &&
                (item1.getStartDate().equals(item2.getStartDate())) &&
                (item1.getEndDate().equals(item2.getEndDate())) &&
                (item1.getReservationDate().equals(item2.getReservationDate())) &&
                (Objects.equals(item1.getCancellationDate(), item2.getCancellationDate())) &&
                (item1.getExtraInfo().equals(item2.getExtraInfo()));
    }

}
